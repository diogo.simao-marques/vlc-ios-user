.. VLC for iOS Documentation master file, created by
   sphinx-quickstart on Mon Sep  9 19:49:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########################
VLC for iOS Documentation
#########################

Welcome to the user documentation of `VLC for iOS <https://www.videolan.org/vlc/download-ios.html>`_, the free and Open Source application that can play all your movies and shows in most formats directly without conversion. 

With VLC for iOS, you can synchronize media to your device using WiFi Upload, iTunes, various Cloud services, or direct downloads from the web.

********
Sections
********

To get the most out of VLC for iOS, start by reviewing a few introductory topics below;

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/index_getting_started.jpg
         :target: gettingstarted/index.html

      :doc:`/gettingstarted/index`
         Quickly find, install and use VLC for iOS on your device.

   .. container:: descr

      .. figure:: /images/index_basic.jpg
         :target: basic/index.html

      :doc:`/basic/index`
         Introduction to the most common uses of VLC for iOS.

   .. container:: descr

      .. figure:: /images/index_advanced.jpg
         :target: advanced/index.html

      :doc:`/advanced/index`
         Advanced scenarios of using VLC for iOS.

   .. container:: descr

      .. figure:: /images/index_reference.jpg
         :target: reference/index.html

      :doc:`/reference/index`
         An (almost) exhaustive description of VLC for iOS features and modules.

   .. container:: descr

      .. figure:: /images/index_faq.jpg
         :target: support/index.html

      :doc:`/support/index`
         Get all the help you need & see the FAQs about VLC for iOS.

   .. container:: descr

      .. figure:: /images/index_glossary.png
         :target: glossary/index.html

      :doc:`/glossary/index`
         Definitions for terms used in VLC for iOS and this documentation.


.. toctree::
   :maxdepth: 1
   :hidden:
   
   gettingstarted/index.rst
   basic/index.rst
   advanced/index.rst
   reference/index.rst
   support/index.rst
   glossary/index.rst
   lore/index.rst
   about.rst
