.. _plex:

####
Plex
####

Plex is a server desktop application that organizes video, audio, and photos from a user's collections and online services, enabling them to access and stream the contents. 
Read more about Plex in this `article <https://en.wikipedia.org/wiki/Plex_(software)>`_. 

This documentation covers how to set up a Plex server and stream media files (audio and video) from that Server directly on VLC for iOS. 

*********************************
How to set up a Plex Media Server
*********************************

We will be using a free Plex solution by `Plex TV <https://www.plex.tv/>`_ to set up the Plex server in this documentation. 

Follow the steps below to set up a Plex Media Server. 

1. Download `Plex Media Server <https://www.plex.tv/media-server-downloads/>`_ and install it on your local machine.

2. Once you open the Plex Media Server on your local machine, a popup modal with a description of how Plex works will be displayed on your screen. When you see this, click on :guilabel:`GOT IT!`. 

.. figure::  /images/advanced/networkshares/plex/how_plex_works.PNG
   :align:   center

3. Add a name for your Server and click on :guilabel:`NEXT`.

.. figure::  /images/advanced/networkshares/plex/server_name.PNG
   :align:   center

4. Click on :guilabel:`ADD LIBRARY` to select the media libraries you want the Server to access. Then click on :guilabel:`NEXT`.

.. figure::  /images/advanced/networkshares/plex/media_library.PNG
   :align:   center

5. Finally, click on :guilabel:`DONE`. You should now have access to your newly created Plex Server. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/advanced/networkshares/plex/done.PNG

   .. container:: descr

      .. figure::  /images/advanced/networkshares/plex/homepage.PNG


********************************************
How to stream media files from a Plex server
********************************************

After setting up a Plex server, you can stream media files (video and audio) directly from VLC for iOS. Here's how;

1. Open VLC for iOS and tap on :guilabel:`Network`.

.. figure::  /images/advanced/networkshares/smb/network_home.jpeg
   :align:   center
   :width: 30%

2. If you installed the Plex Media Server on your local machine, the server will be detected by VLC and displayed on your screen. Tap on the detected Plex server to access your media files.

.. figure::  /images/advanced/networkshares/plex/plex_server.jpeg
   :align:   center
   :width: 30%

3. Tap on your preferred media folder to see all the media files you have access to on the Plex server. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/advanced/networkshares/plex/media_folder.jpeg

   .. container:: descr

      .. figure::  /images/advanced/networkshares/plex/media_files.jpeg


4. On seeing your preferred media file, tap on it, and enjoy streaming.

.. figure::  /images/advanced/networkshares/plex/preferred_media_file.jpeg
   :align:   center
   :width: 30%
