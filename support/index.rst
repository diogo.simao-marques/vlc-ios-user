.. _support:

*******
Support
*******

The VLC Support Guide is an informal, step-by-step guide for troubleshooting the most common issues with VLC. It also contains frequently asked questions about
VideoLAN, and VLC for iOS. 

.. toctree::
   :maxdepth: 1
   :name: toc-Support
   
   faq/index.rst
   gethelp.rst
   report_a_bug.rst

