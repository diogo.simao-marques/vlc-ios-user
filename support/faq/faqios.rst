.. _faq_ios:

***********
VLC for iOS
***********

Find below frequently asked questions about VLC on iOS:

How can I hide my videos?
+++++++++++++++++++++++++

You can hide your videos on VLC for iOS by adding a **Passcode Lock**. With this, only people with the passcode would be able to access the videos on your VLC for iOS application. 
To enable a passcode on VLC for iOS, tap on :menuselection:`Settings --> Passcode Lock --> Add your passcode.`


.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/support/faq/passcodelock.jpeg

I forgot my passcode. How can I retrieve my passcode?
+++++++++++++++++++++++++++++++++++++++++++++++++++++

If you forget your passcode, you need to delete the application and re-install it.

How can I add another passcode?
+++++++++++++++++++++++++++++++

To add another passcode, disable the Passcode Lock in VLC's Settings and re-enable it. When you do this, you will be asked to re-enter another passcode. 

Is VLC available for iPhones?
+++++++++++++++++++++++++++++

Yes, VLC for iOS can play all your movies and shows in most formats directly without conversion. 
You can also synchronize media to your device using WiFi Upload, iTunes, various Cloud services, or direct downloads from the web. Read more about this in the :doc:`/gettingstarted/media_synchronization` documentation.

How can I install VLC for iOS on my device?
+++++++++++++++++++++++++++++++++++++++++++

We explained the entire process in the :doc:`/gettingstarted/setup` section of this documentation. 

What are the supported codecs on VLC for iOS?
+++++++++++++++++++++++++++++++++++++++++++++

* **Video Codec**:	mp4v, H264 (up to `2500 kbps` and `640x480 px`)
* **Audio Codec**:	mp4a, aac (up to `160 kbps`)
* **Container**:	mp4
* **Screen size**:	480x320px (1.5:1 or 3:2)

How can I download a video or audio?
++++++++++++++++++++++++++++++++++++

Open up your VLC for iOS application, go to the :guilabel:`Network` tab and tap on :guilabel:`Downloads`. Enter the URL address of the file you want to download and tap on the :guilabel:`Download` button. A progress bar will indicate when the download is complete.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/gettingstarted/mediasynchronization/downloads.jpg

   .. container:: descr

      .. figure:: /images/gettingstarted/mediasynchronization/downloads_main.jpeg

How to add files to VLC on your iPhone without iTunes
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Aside from adding files using iTunes, you can use any of our :doc:`/gettingstarted/media_synchronization` methods to add media files to VLC.